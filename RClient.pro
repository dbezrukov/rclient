#-------------------------------------------------
#
# Project created by QtCreator 2013-03-26T22:34:44
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = rclient
TEMPLATE = app

SOURCES += main.cpp\
        rclientdialog.cpp \
        rtask.cpp \
        qt-json/json.cpp \
        api/serviceinforequest.cpp \
        api/servicestartrequest.cpp \
    networkmanager.cpp

HEADERS += rclientdialog.h \
        rtask.h \
        api/serviceinforequest.h \
        api/servicestartrequest.h \
    networkmanager.h

FORMS   += rclientdialog.ui
