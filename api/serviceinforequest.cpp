#include "api/serviceinforequest.h"
#include "networkmanager.h"
#include "qt-json/json.h"

ServiceInfoRequest::ServiceInfoRequest(const QString& serviceUri)
    : QObject(NetworkManager::instance())
    , reply(0)
{
    setUrl(serviceUri);
    setRawHeader("Accept", "application/json");
}

void ServiceInfoRequest::start()
{
    reply = NetworkManager::instance()->get(*this);
    connect(reply, SIGNAL(readyRead()), this, SLOT(readyRead()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(error(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), this, SLOT(finished()));
}

void ServiceInfoRequest::readyRead()
{
    QByteArray data = reply->readAll();

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if(ok) {
        emit done(result["description"].toString());
    }
}

void ServiceInfoRequest::error(QNetworkReply::NetworkError)
{
    emit failed(QString("Service not found, please check the app config"));
}

void ServiceInfoRequest::finished()
{
    reply->deleteLater();
    delete this;
}
