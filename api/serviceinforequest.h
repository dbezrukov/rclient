#ifndef SERVICEINFOREQUEST_H
#define SERVICEINFOREQUEST_H

#include <QNetworkRequest>
#include <QNetworkReply>

//! Запрос информации о вычислительном сервисе в облаке MathCloud
/*!
    Класс реализует запрос информации о вычислительном сервисе в соотвествии с протоколом RestAPI.
*/
class ServiceInfoRequest : public QObject, public QNetworkRequest
{
    Q_OBJECT
public:
    //! Конструктор класса
    /*!
       \param serviceUri уникальный адрес сервиса в облаке MathCloud
    */
    explicit ServiceInfoRequest(const QString& serviceUri);
    
signals:
    //! Сигнал о получении информации о сервисе
    /*!
       \param info информация о сервисе
    */
    void done(QString info);

    //! Сигнал об ошибке получения информации о сервисе
    /*!
       \param error описание ошибки
    */
    void failed(QString error);
    
public slots:
    //! Запуск запроса информации о сервисе
    void start();

protected slots:
    //! Обработчик информации о сервисе
    void readyRead();
    //! Обработчик ошибки сетевого запроса
    /*!
       \param code код ошибки
    */
    void error(QNetworkReply::NetworkError code);
    //! Обработчик завершения сетевого запроса
    void finished();

private:
    QNetworkReply* reply; //!< Дискриптор операции запроса на информацию о сервисе
};

#endif // SERVICEINFOREQUEST_H
