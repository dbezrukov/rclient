#include "api/servicestartrequest.h"
#include "networkmanager.h"
#include "qt-json/json.h"

#include <QHttpMultiPart>
#include <QTimer>
#include <QDebug>

ServiceStartRequest::ServiceStartRequest(const QString& serviceUri)
    : QObject(NetworkManager::instance())
    , serviceUri(serviceUri)
    , resultsReceived(false)
    , replyUpload(0)
    , replyStart(0)
{
}

void ServiceStartRequest::start(QFile* file)
{
    setUrl(serviceUri + "/upload");

    QHttpMultiPart* multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);
    QHttpPart filePart;

    QString contentDisposition("form-data; name=\"file\"; filename=\"%1\"");
    contentDisposition = contentDisposition.arg(file->fileName());

    filePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/octet-stream"));
    filePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant(contentDisposition));
    filePart.setBodyDevice(file);
    file->setParent(multiPart);
    multiPart->append(filePart);

    // upload
    replyUpload = NetworkManager::instance()->post(*this, multiPart);
    multiPart->setParent(replyUpload);

    connect(replyUpload, SIGNAL(readyRead()), this, SLOT(readyReadUpload()));
    connect(replyUpload, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(requestError(QNetworkReply::NetworkError)));
    connect(replyUpload, SIGNAL(finished()), this, SLOT(requestFinished()));
}

void ServiceStartRequest::readyReadUpload()
{
    QByteArray data = replyUpload->readAll();

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if ( ok ) {
        QString fileUri = result["uri"].toString();

        QVariantMap dataMap;
        dataMap.insert("in", fileUri);

        setUrl(serviceUri);
        setRawHeader("Accept", "application/json");
        setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));

        replyStart = NetworkManager::instance()->post(*this, QtJson::serialize(dataMap));
        connect(replyStart, SIGNAL(readyRead()), this, SLOT(readyReadStart()));
        connect(replyStart, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(requestError(QNetworkReply::NetworkError)));
        connect(replyStart, SIGNAL(finished()), this, SLOT(requestFinished()));
    }
}

void ServiceStartRequest::readyReadStart()
{
    QByteArray data = replyStart->readAll();

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if ( ok ) {
        jobUri = replyStart->rawHeader("Location");
        processJobResults(result);
    }
}

void ServiceStartRequest::readyReadResults()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    QByteArray data = reply->readAll();

    bool ok;
    QVariantMap result = QtJson::parse(data, ok).toMap();

    if ( ok ) {
        processJobResults(result);
    }
}

void ServiceStartRequest::queryResults()
{
    setUrl(jobUri);
    setRawHeader("Accept", "application/json");
    setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/json"));

    QNetworkReply* reply = NetworkManager::instance()->get(*this);
    connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadResults()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(requestError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(finished()), this, SLOT(requestFinished()));
}

void ServiceStartRequest::requestError(QNetworkReply::NetworkError)
{
    emit failed(QString("Service not found, please check the app config"));
}

void ServiceStartRequest::requestFinished()
{
    QNetworkReply* reply = qobject_cast<QNetworkReply*>(sender());
    reply->deleteLater();

    if (resultsReceived) {
        delete this;
    }
}

void ServiceStartRequest::processJobResults(QVariantMap& data)
{
    QString state = data["state"].toString();

    QVariantMap results = data["result"].toMap();
    QString out = results["out"].toString();

    if (!out.isEmpty()) {
        emit done(state, out);
        resultsReceived = true;
    } else {
        emit done(state, "querying..");
        QTimer::singleShot(2000, this, SLOT(queryResults()));
    }
}
