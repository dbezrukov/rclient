#ifndef SERVICESTARTREQUEST_H
#define SERVICESTARTREQUEST_H

#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFile>

//! Запуск и отслеживание результатов выполнения вычислительного сервиса в облаке MathCloud
/*!
    Класс реализует запуск и отслеживание результатов выполнения задачи в соотвествии с протоколом RestAPI.
*/
class ServiceStartRequest : public QObject, public QNetworkRequest
{
    Q_OBJECT
public:
    //! Конструктор класса
    /*!
       \param serviceUri Уникальный адрес сервиса в облаке MathCloud
    */
    explicit ServiceStartRequest(const QString& serviceUri);

signals:
    //! Сигнал об изменении статуса выполнения задачи
    /*!
       \param status текущий статус в соотвествии с протоколом RestAPI
       \param results результаты
    */
    void done(QString status, QString results);

    //! Сигнал об ошибке запуска сервиса
    /*!
       \param error описание ошибки
    */
    void failed(QString error);

public slots:
    //! Запуск выполнения расчетов
    /*!
       \param file файл с исходными данными
    */
    void start(QFile* file);

protected slots:
    //! Обработчик результата загрузки файла входных данных
    void readyReadUpload();
    //! Обработчик результата запуска расчетной задачи
    void readyReadStart();
    //! Обработчик текущего статуса выполнения задачи
    void readyReadResults();

    //! Запрос статуса выполнения задачи
    void queryResults();

    //! Обработчик ошибки сетевого запроса
    /*!
       \param code код ошибки
    */
    void requestError(QNetworkReply::NetworkError code);
    //! Обработчик завершения сетевого запроса
    void requestFinished();

protected:
    //! Обработчик результатов выполнения вычислительной задачи
    /*!
       \param data JSON структура с результатами выполнения задачи
    */
    void processJobResults(QVariantMap& data);

private:
    QString serviceUri; //!< Уникальный адрес сервиса в облаке MathCloud
    QString jobUri; //!< Уникальный адрес запущенной задачи в облаке MathCloud
    bool resultsReceived; //!< Признак получения результатов задачи
    QNetworkReply* replyUpload; //!< Дискриптор операции загрузки файла с данными для задачи
    QNetworkReply* replyStart; //!< Дискриптор операции запуска вычислительной задачи
};

#endif // SERVICESTARTREQUEST_H
