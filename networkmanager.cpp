#include "networkmanager.h"

NetworkManager* NetworkManager::m_instance = NULL;

NetworkManager::NetworkManager()
    : QNetworkAccessManager(0)
{
}

NetworkManager* NetworkManager::instance()
{
    if (m_instance == NULL) {
        m_instance = new NetworkManager();
        Q_ASSERT(m_instance);
    }
    return m_instance;
}

void NetworkManager::close()
{
    if (m_instance) {
        m_instance->deleteLater();
        m_instance = 0;
    }
}
