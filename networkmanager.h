#ifndef NETWORKMANAGER_H
#define NETWORKMANAGER_H

#include <QString>
#include <QNetworkAccessManager>

//! Менеджер сетевых соединений
class NetworkManager : public QNetworkAccessManager
{
    Q_OBJECT

public:
    //! Создание экземпляра менеджера
    /*!
       \return Экземпляр менеджера
    */
    static NetworkManager * instance();

    //! Деактивация менеджера
    void close();

protected:
    //! Конструктор класса
    NetworkManager();
    //! Деструктор класса
    virtual ~NetworkManager() {}

private:
    static NetworkManager * m_instance; //!< Экземпляр менеджера сетевых соединений

    Q_DISABLE_COPY(NetworkManager)
};

#endif // NETWORKMANAGER_H
