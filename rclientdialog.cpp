#include "rclientdialog.h"
#include "ui_rclientdialog.h"

#include <QFileDialog>
#include <QSettings>

RClientDialog::RClientDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::RClientDialog)
{
    setupUi();
    setupRTask();
}

RClientDialog::~RClientDialog()
{
    delete ui;

}

void RClientDialog::setupUi()
{
    ui->setupUi(this);
    ui->startTaskBtn->setEnabled(false);
    connect(ui->fileSelectBtn, SIGNAL(clicked()), this, SLOT(selectFile()));
    connect(ui->startTaskBtn, SIGNAL(clicked()), this, SLOT(startTask()));
}

void RClientDialog::setupRTask()
{
    /// чтение настроек приложения из ini-файла
    QSettings settings(QCoreApplication::applicationDirPath() + "/rclient.ini", QSettings::IniFormat);

    QVariant serviceUri = settings.value("serviceUri");
    if (serviceUri.isNull()) {
        ui->serviceInfo->appendPlainText("Please specify the Service URI value in the app config (serviceUri)");
        return;
    }

    task.reset(new RTask(serviceUri.toString(), this));
    connect(task.data(), SIGNAL(serviceInfoAvailable(QString)), ui->serviceInfo, SLOT(appendPlainText(QString)));
    connect(task.data(), SIGNAL(serviceStatusChanged(QString,QString)), this, SLOT(serviceStatusChanged(QString,QString)));
    connect(task.data(), SIGNAL(serviceInfoFailed(QString)), this, SLOT(serviceStartFailed(QString)));

    /// запрос информации о сервисе из MathCloud
    task->queryServiceInfo();

    /// автоматический запуск задачи при наличии исходных данных
    QVariant fileName = settings.value("inputFile");
    if (!fileName.isNull()) {
        setFileName(fileName.toString());
        startTask();
    }
}

void RClientDialog::selectFile()
{
    QString fileName = QFileDialog::getOpenFileName(
                            this,
                            "Select file with entries",
                            "",
                            "fer4cnf (*.fer4cnf)");
    setFileName(fileName);
}

void RClientDialog::startTask()
{
    ui->taskStatus->setText("Starting..");
    ui->result->clear();

    task->startService(ui->fileInfo->text());
}

void RClientDialog::serviceStatusChanged(QString status, QString results)
{
    ui->taskStatus->setText(status);
    ui->result->appendPlainText(results);
}

void RClientDialog::serviceStartFailed(QString error)
{
    ui->serviceInfo->appendPlainText(error);
    ui->taskStatus->setText("Service not found");
}

void RClientDialog::setFileName(const QString& fileName)
{
    if (!fileName.isEmpty()) {
        ui->fileInfo->setText(fileName);
        ui->startTaskBtn->setEnabled(true);
    }
}
