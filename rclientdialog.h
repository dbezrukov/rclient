#ifndef RCLIENTDIALOG_H
#define RCLIENTDIALOG_H

#include "rtask.h"

#include <QDialog>
#include <QScopedPointer>

namespace Ui {
class RClientDialog;
}

//! Главное окно программы
class RClientDialog : public QDialog
{
    Q_OBJECT
    
public:
    //! Конструктор класса
    explicit RClientDialog(QWidget *parent = 0);
    //! Деструктор класса
    ~RClientDialog();

protected:
    //! Инициализация пользовательского интерфейса
    void setupUi();
    //! Инициализация класса вычислительной задачи
    void setupRTask();
    //! Задание имени файла с входными данными для задачи
    /*!
       \param name имя файла с входными данным
    */
    void setFileName(const QString& name);

signals:
    //! Сигнал о необходимости запуска вычислительной задачи
    /*!
       \param name имя файла с входными данным
    */
    void startService(QString name);

private slots:
    //! Обработчик выбора файла из пользовательского интерфейса
    void selectFile();
    //! Обработчик запуска задачи из пользовательского интерфейса
    void startTask();
    //! Обработчик изменения статуса запущенной задачи
    /*!
       \param status текущий статус
       \param results результаты выполнения задачи
    */
    void serviceStatusChanged(QString status, QString results);
    //! Обработчик ошибки запуска задачи
    /*!
       \param error описание ошибки
    */
    void serviceStartFailed(QString error);

private:
    Ui::RClientDialog *ui; //!< Экземпляр класса описания пользовательского интерфейса
    QScopedPointer<RTask> task; //!< Экземпляр вычислительной задачи
};

#endif // RCLIENTDIALOG_H
