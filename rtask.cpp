#include "rtask.h"
#include "api/serviceinforequest.h"
#include "api/servicestartrequest.h"
#include "networkmanager.h"

#include <QFile>
#include <QDebug>
#include <QMessageBox>
#include <QCoreApplication>

RTask::RTask(const QString& serviceUri, QObject *parent)
    : QObject(parent)
    , serviceUri(serviceUri)
{
}

RTask::~RTask()
{
    NetworkManager::instance()->close();
}

void RTask::queryServiceInfo()
{
    ServiceInfoRequest* serviceInfoRequest = new ServiceInfoRequest(serviceUri);
    connect(serviceInfoRequest, SIGNAL(done(QString)), this, SIGNAL(serviceInfoAvailable(QString)));
    connect(serviceInfoRequest, SIGNAL(failed(QString)), this, SIGNAL(serviceInfoFailed(QString)));
    serviceInfoRequest->start();
}

void RTask::startService(QString fileName)
{
    qDebug() << "Starting service:"
             << "uri: " << serviceUri
             << ", fileName: " << fileName;

    QFile* file = new QFile(fileName);
    if (!file->open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox msgBox;
        msgBox.setText(QString("Can't open file: %1").arg(fileName));
        msgBox.exec();
        return;
    }

    ServiceStartRequest* serviceStartRequest = new ServiceStartRequest(serviceUri);
    connect(serviceStartRequest, SIGNAL(done(QString, QString)), this, SIGNAL(serviceStatusChanged(QString, QString)));
    connect(serviceStartRequest, SIGNAL(failed(QString)), this, SIGNAL(serviceStartFailed(QString)));
    serviceStartRequest->start(file);
}
