#ifndef RTASK_H
#define RTASK_H

#include <QObject>

//! Вычислительная задача
/*!
    Класс обеспечивает:
        - получение информации об используемом сервисе
        - запуск задачи с передачей файла входных данных
        - выдачу промежуточного статуса выполнения задачи
        - возврат результатов вычисления
*/
class RTask : public QObject
{
    Q_OBJECT

public:
    //! Конструктор класса
    /*!
       \param serviceUri Уникальный адрес сервиса
       \param parent Владелец объекта
    */
    explicit RTask(const QString& serviceUri, QObject* parent = 0);
    //! Деструктор класса
    virtual ~RTask();
    
public:
    //! Запрос информации о сервисе из MathCloud
    void queryServiceInfo();

    //! Запуск задачи
    /*!
       \param fileName имя файла с входными данным
    */
    void startService(QString fileName);

signals:
    //! Сигнал о получении информации о сервисе
    /*!
       \param info строка информации
    */
    void serviceInfoAvailable(QString info);

    //! Сигнал об ошибке получении информации о сервисе
    /*!
       \param error описание ошибки
    */
    void serviceInfoFailed(QString error);

    //! Сигнал об изменении статуса выполнения задачи
    /*!
       \param status текущий статус согласно протоколу RestAPI: WAITING,RUNNING,DONE,FAILED
       \param results результат расчетов, если расчеты завершены
    */
    void serviceStatusChanged(QString status, QString results);

    //! Сигнал об ошибке запуска задачи
    /*!
       \param error описание ошибки
    */
    void serviceStartFailed(QString error);

protected:
    QString serviceUri; //!< Уникальный адрес сервиса в облаке MathCloud
};

#endif // RTASK_H
