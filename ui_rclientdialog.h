/********************************************************************************
** Form generated from reading UI file 'rclientdialog.ui'
**
** Created: Wed Mar 27 11:38:29 2013
**      by: Qt User Interface Compiler version 4.8.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_RCLIENTDIALOG_H
#define UI_RCLIENTDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPlainTextEdit>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_RClientDialog
{
public:
    QVBoxLayout *verticalLayout;
    QLabel *serviceInfoLabel;
    QPlainTextEdit *serviceInfo;
    QLabel *fileInfoLabel;
    QHBoxLayout *horizontalLayout;
    QLineEdit *fileInfo;
    QPushButton *fileSelectBtn;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *startTaskBtn;
    QLabel *taskStatusLabel;
    QLabel *taskStatus;
    QLabel *resultLabel;
    QPlainTextEdit *result;

    void setupUi(QDialog *RClientDialog)
    {
        if (RClientDialog->objectName().isEmpty())
            RClientDialog->setObjectName(QString::fromUtf8("RClientDialog"));
        RClientDialog->setWindowModality(Qt::ApplicationModal);
        RClientDialog->resize(552, 464);
        RClientDialog->setSizeGripEnabled(true);
        RClientDialog->setModal(false);
        verticalLayout = new QVBoxLayout(RClientDialog);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        serviceInfoLabel = new QLabel(RClientDialog);
        serviceInfoLabel->setObjectName(QString::fromUtf8("serviceInfoLabel"));

        verticalLayout->addWidget(serviceInfoLabel);

        serviceInfo = new QPlainTextEdit(RClientDialog);
        serviceInfo->setObjectName(QString::fromUtf8("serviceInfo"));
        serviceInfo->setMaximumSize(QSize(16777215, 60));

        verticalLayout->addWidget(serviceInfo);

        fileInfoLabel = new QLabel(RClientDialog);
        fileInfoLabel->setObjectName(QString::fromUtf8("fileInfoLabel"));

        verticalLayout->addWidget(fileInfoLabel);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        fileInfo = new QLineEdit(RClientDialog);
        fileInfo->setObjectName(QString::fromUtf8("fileInfo"));
        fileInfo->setAutoFillBackground(false);
        fileInfo->setReadOnly(true);

        horizontalLayout->addWidget(fileInfo);

        fileSelectBtn = new QPushButton(RClientDialog);
        fileSelectBtn->setObjectName(QString::fromUtf8("fileSelectBtn"));

        horizontalLayout->addWidget(fileSelectBtn);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        startTaskBtn = new QPushButton(RClientDialog);
        startTaskBtn->setObjectName(QString::fromUtf8("startTaskBtn"));

        horizontalLayout_2->addWidget(startTaskBtn);

        taskStatusLabel = new QLabel(RClientDialog);
        taskStatusLabel->setObjectName(QString::fromUtf8("taskStatusLabel"));

        horizontalLayout_2->addWidget(taskStatusLabel);

        taskStatus = new QLabel(RClientDialog);
        taskStatus->setObjectName(QString::fromUtf8("taskStatus"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(1);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(taskStatus->sizePolicy().hasHeightForWidth());
        taskStatus->setSizePolicy(sizePolicy);

        horizontalLayout_2->addWidget(taskStatus);


        verticalLayout->addLayout(horizontalLayout_2);

        resultLabel = new QLabel(RClientDialog);
        resultLabel->setObjectName(QString::fromUtf8("resultLabel"));
        resultLabel->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        verticalLayout->addWidget(resultLabel);

        result = new QPlainTextEdit(RClientDialog);
        result->setObjectName(QString::fromUtf8("result"));

        verticalLayout->addWidget(result);


        retranslateUi(RClientDialog);

        QMetaObject::connectSlotsByName(RClientDialog);
    } // setupUi

    void retranslateUi(QDialog *RClientDialog)
    {
        RClientDialog->setWindowTitle(QApplication::translate("RClientDialog", "RService client", 0, QApplication::UnicodeUTF8));
        serviceInfoLabel->setText(QApplication::translate("RClientDialog", "Service description", 0, QApplication::UnicodeUTF8));
        fileInfoLabel->setText(QApplication::translate("RClientDialog", "Entries", 0, QApplication::UnicodeUTF8));
        fileInfo->setText(QString());
        fileSelectBtn->setText(QApplication::translate("RClientDialog", "Load file", 0, QApplication::UnicodeUTF8));
        startTaskBtn->setText(QApplication::translate("RClientDialog", "Start calc", 0, QApplication::UnicodeUTF8));
        taskStatusLabel->setText(QApplication::translate("RClientDialog", "Status:", 0, QApplication::UnicodeUTF8));
        taskStatus->setText(QApplication::translate("RClientDialog", "waiting", 0, QApplication::UnicodeUTF8));
        resultLabel->setText(QApplication::translate("RClientDialog", "Results", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class RClientDialog: public Ui_RClientDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_RCLIENTDIALOG_H
